*** Settings ***
#| Suite Setup | Set Library Search Order | AppiumLibrary | Selenium2Library
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables    var/variables.py


*** Keywords ***

Relaunch Application
    Close Application
    Sleep    5s    
    Launch FacePay Vendor Application

# SMS App
Open SMS
    Open Application    http://127.0.1.1:4723/wd/hub    platformName=Android    platformVersion=8.1    deviceName=0081889b0804    appPackage=com.android.mms    appActivity=com.android.mms.ui.ConversationList
    Sleep    5s 

# FacePay  Application
Launch FacePay Vendor Application
   Open Application    http://127.0.1.1:4723/wd/hub    platformName=Android    platformVersion=8.1    deviceName=0081889b0804    appPackage=com.rncameraexample    appActivity=com.rncameraexample.MainActivity
   Sleep    5s