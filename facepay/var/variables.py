def get_variables(arg=None):
    variables = {
        'var' : 'value',
        'LoginTitle' :             u'Welcome to FacePay!',
        'CreateAccountTitle' :     u'Create an account',
        'ValidateTitle'      :     u'Validate OTP',
        'patternTitle':            u'Draw an unlock pattern',
        'AppName':                 u'FacePay',
        'Name':                    u'Test User',
        'Email':                   u'asiddarth86@gmail.com',
        'Password':                u'123456',
        'PhoneNumber':             u'8861835846',
        'NewName':                 u'New Test User',
        'NewEmail':                u's..h.a.r.p.t.h.e.e.@gmail.com',
        'NewPassword':             u'123456',
        'NewPhoneNumber':          u'7019847480',  
        'WrongEmail':              u'a$idd@rth@gmail.com',
        'WrongPassword':           u'1234567'  
        }
    return variables
