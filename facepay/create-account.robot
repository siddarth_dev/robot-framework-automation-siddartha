*** Settings ***
| Suite Setup | Set Library Search Order | AppiumLibrary | Selenium2Library
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables    var/variables.py


*** Keywords ***

create account test
    Log    ${LoginTitle}
    
Create Account Should Fail
    
    Sleep    2s    
    Log    ${LoginTitle}   
    Page Should Contain Text   ${LoginTitle} 
      
    # click Sign up
    Click Element    xpath=//*[@text=\"Sign Up\"]
    
    Wait Until Page Contains    ${CreateAccountTitle}
    
    # Enter test user details
    Input Text   accessibility_id=create_ac_name            ${Name}         
    Input Text   accessibility_id=create_ac_email           ${WrongEmail}
    Input Text   accessibility_id=create_ac_password        ${Password}
    Input Text   accessibility_id=create_ac_phone_number    ${PhoneNumber}   
           
    Click Element    xpath=//*[@text=\"Next\"]
    Sleep   5s    
     
    # Verfiy for successfull request
    ${toast}     Get Text    accessibility_id=toast    
    
    Should Not Be Equal     ${toast}    None    
    Sleep    5s    
    Log    ${toast}     
    
    
Create Account Should Pass
    
    Sleep    2s    
    Page Should Contain Text   ${LoginTitle} 
      
    # click Sign up
    Click Element    xpath=//*[@text=\"Sign Up\"]
    
    Wait Until Page Contains    ${CreateAccountTitle}         
        
    # Enter test user details
    Input Text   accessibility_id=create_ac_name            ${NewName}         
    Input Text   accessibility_id=create_ac_email           ${NewEmail}
    Input Text   accessibility_id=create_ac_password        ${NewPassword}
    Input Text   accessibility_id=create_ac_phone_number    ${NewPhoneNumber}   
           
    Click Element    xpath=//*[@text=\"Next\"]
    Sleep   5s    
     
    # Verfiy for successfull request
    ${toast}     Get Text    accessibility_id=toast 
    Log    ${toast}    
    
    Should Be Equal    ${toast}    None    
    Sleep    5s    
    
    # verify navigated to OTP page
    Page Should Contain Text    Validate OTP
    Sleep    5s    
    
    # Put FacePay app in background
    Background App    10
    
    
    # Launch SMS App
    Start Activity   com.android.mms    com.android.mms.ui.ConversationList
    Sleep    5s
    

    # Read OTP
    ${latestMessage}     Get Text    id=com.android.mms:id/conversation_snippet
    Log     ${latestMessage}    
    
    # Verify OTP came from FacePay messaging service
    Should Contain    ${latestMessage}    is your verification code for FacePay 
    
    # Extract only otp from message
    @{otp}     Split String   ${latestMessage}     
    Log    @{otp}[0] 
    
    
    # Switch back to FacePay app
    Press Keycode    187
    Click Element    accessibility_id=FacePay    
     
    Sleep    2s
    
  
    # Verify switched to Facepay app
    Page Should Contain Text    Validate OTP
           
    Input Text   accessibility_id=confirm_ac_enter_otp      @{otp}[0]
    Sleep    2s
    
        
    Click Element    xpath=//*[@text=\"Validate\"]
    Sleep    2s    
    