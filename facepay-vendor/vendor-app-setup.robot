*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables    variables/variable.py


*** Keywords ***

Relaunch Application
    Close Application
    Sleep    3s    
    Launch FacePay Vendor Application
    
Open SMS Application
    Log  Opening sms application
    Open SMS
        
Start Message Application
    start activity  com.google.android.apps.messaging  com.google.android.apps.messaging.ui.ConversationList
    sleep  10s
# SMS App
Open SMS
    Open Application    http://127.0.1.1:4723/wd/hub    platformName=Android    platformVersion=8.1    deviceName=0081889b0804    appPackage=com.google.android.apps.messaging    appActivity=com.google.android.apps.messaging.ui.ConversationList
    Sleep    5s 

# FacePay Vendor Application
Launch FacePay Vendor Application
   Open Application    http://127.0.1.1:4723/wd/hub    platformName=Android    platformVersion=8.1    deviceName=0081889b0804    appPackage=com.inqude.facepayvendor    appActivity=com.inqude.facepayvendor.MainActivity
   Sleep    5s