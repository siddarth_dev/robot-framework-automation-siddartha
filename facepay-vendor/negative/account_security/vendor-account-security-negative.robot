*** Settings ***
| Suite Setup | Set Library Search Order | AppiumLibrary | Selenium2Library
Library  AppiumLibrary  10
Library  String    
Library  OperatingSystem 


Resource    keywords/choose-security-pin-negative.robot
Resource    ../../positive/account/keywords/facepay-vendor-login-positive.robot
Resource    ../../vendor-app-setup.robot

*** Test Cases ***
Launch FacePay Vendor Application
    Log  Launch FacePay Vendor Application
    Launch FacePay Vendor Application
    
Login Application with Valid Credentials
    Log  Login Application with Valid credentails
    User login with valid credentials
    
Choose Security Pin to Secure the Account With Wrong Entries
    Log  User need to set the security pin to secure the Account
    Set a Security Pin for First Time With Two Wrong Entries
    

    