*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables   ../../../variables/variable.py


*** Keywords ***
Change a Security Pin With Wrong Number
    
    sleep  2s
    
    Click element  accessibility_id=logout_btn
    sleep  2s
    
    Click element  xpath=//*[@text=\"Change Pin\"]
    sleep  2s
    
    Input text  accessibility_id=old_pin   ${WrongSecurityCode}
    sleep  2s
    
    Input text  accessibility_id=new_pin   ${OldPin}
    sleep  2s
    
    Click element  xpath=//*[@text=\"Change\"]
    sleep  2s
    
    Press Keycode  \13
    sleep  2s
    
    Page Should Not Contain Text     Recent Transactions
    sleep  2s
    