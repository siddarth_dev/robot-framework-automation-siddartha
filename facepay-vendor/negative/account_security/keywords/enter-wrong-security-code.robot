*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables   ../../../variables/variable.py



*** Keywords ***
Enter Security Pin To Re-enter into App
    
    sleep  4s
    Wait Until Page Contains Element  //*[@text=\"Enter Security pin\"]
    sleep  2s
    
    
    Input text  accessibility_id=security_pin   ${WrongSecurityCode}
    sleep  2s
    
    Tap  xpath=//*[@text=\"Validate\"]
    sleep  4s
    
    Wait Until Page Contains Element  //*[@text=\"Hello!\"]
    sleep  2s