*** Settings ***
Library     AppiumLibrary    10
Library     String    
Library     OperatingSystem 
Variables   ../../../variables/variable.py

*** Keywords ***
Set a Security Pin for First Time With Two Wrong Entries
    
    sleep  2s
    
    Input text  accessibility_id=pin   ${SecurityCode}
    sleep  2s
    
    Input text  accessibility_id=confirm_pin   ${WrongSecurityCode}
    sleep  2s
    
    Click element  xpath=//*[@text=\"Set"\]
    sleep  5s
    
    Page Should Not Contain Text  Recent Transactions
    sleep  2s