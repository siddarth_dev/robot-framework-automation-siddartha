*** Settings ***
| Suite Setup | Set Library Search Order | AppiumLibrary | Selenium2Library
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Variables   ../../variables/variable.py
Resource    ../../vendor-app-setup.robot
Resource    keywords/facepay-vendor-login-negative.robot

 

*** Test Cases ***
Launch FacePay Vendor Application
    Log  Launch FacePay Vendor Application
    Launch FacePay Vendor Application
        
User Login with invalid email
    Log  User try to login with invalid email
    User login with InValid Email
    
User Login with invalid password
    Log  User try to login with invalid password
    Relaunch Application
    User login with Invalid Password
    
User Login without username and with password
    Log  User try to login without username & with passowrd
    Relaunch Application
    User Login Without Username
    
User Login with username and without password
    Log  User try to login with username & without password
    Relaunch Application
    User Login Without Password
