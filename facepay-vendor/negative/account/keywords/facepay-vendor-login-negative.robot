*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables  ../../../variables/variable.py


*** Keywords ***

User login with InValid Credentials
    
    sleep  5s
    Log  ${LoginWindowTitle}
    Page Should Contain Text  ${LoginWindowTitle}
    
    Input text  accessibility_id=login_email         ${InvalidEmail}
    Input text  accessibility_id=login_password      ${InvalidPassword}
    sleep  2s
    
    Click Element  xpath=//*[@text=\"Login\"]
    sleep  4s
    
    
    ${toast}     Get Text    accessibility_id=toast
    
    Should Not Be Equal  ${toast} none
    sleep  2s
    
    
User login with InValid Email
    
    sleep  5s
    Log  ${LoginWindowTitle}
    Page Should Contain Text  ${LoginWindowTitle}
    
    Input text  accessibility_id=login_email         ${InvalidEmail}
    Input text  accessibility_id=login_password      ${ValidPassword}
    sleep  2s
    
    Click Element  xpath=//*[@text=\"Login\"]
    sleep  4s
    
    
    ${toast}     Get Text    accessibility_id=toast
    
    Should Not Be Equal   ${toast}  None
    sleep  2s
    
    
User login with Invalid Password
    
    sleep  5s
    Log  ${LoginWindowTitle}
    Page Should Contain Text  ${LoginWindowTitle}
    
    Input text  accessibility_id=login_email         ${ValidEmail}
    Input text  accessibility_id=login_password      ${InvalidPassword}
    sleep  2s
    
    Click Element  xpath=//*[@text=\"Login\"]
    sleep  4s
    
    
    ${toast}     Get Text    accessibility_id=toast
    
User Login Without Username
    
    sleep  2s
    Log  ${LoginWindowTitle}
    Page Should Contain Text  ${LoginWindowTitle}
    
    Input text  accessibility_id=login_email    ${NoEmail}
    Input text  accessibility_id=login_password  ${ValidPassword}
    sleep  2s
    
    Tap  xpath=//*[@text=\"Login\"]
    sleep  2s
    
    Page Should Not Contain Element  //*[@text=\"Choose Security pin\"]
        
    
    
User Login Without Password
    
    sleep  2s
    Log  ${LoginWindowTitle}
    Page Should Contain Text  ${LoginWindowTitle}
    
    Input text  accessibility_id=login_email    ${ValidEmail}
    Input text  accessibility_id=login_password  ${NoPassword}
    sleep  2s
    
    Tap  xpath=//*[@text=\"Login\"]
    sleep  2s
    
    Page Should Not Contain Element  //*[@text=\"Choose Security pin\"]