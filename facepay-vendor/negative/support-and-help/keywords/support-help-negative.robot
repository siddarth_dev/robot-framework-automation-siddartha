*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables  ../../../variables/variable.py


*** Keywords ***
Report a Issue in Support & Help Without Typing Any Message
    
    sleep  2s
    
    Click element  accessibility_id=logout_btn
    sleep  2s
    
    Click element  xpath=//*[@text=\"Support & Help"\]
    sleep  4s
    
    Wait Until Page Contains Element  //*[@text=\"Report new Issue"\]
    sleep  2s
    
    Tap  xpath=//*[@text=\"Report new Issue"\]
    sleep  3s
    
    Click element  xpath=//*[@text=\"Can't make any transactions"\]
    sleep  3s
    
    Click element  xpath=//*[@text=\"Face is not recognizing properly"\]
    sleep  3s
    
    Input text  accessibility_id=create_ticket_summary   ${NoIssueMsg}
    sleep  3s
    
    Element Should Be Disabled  //*[@text=\"Report"\]
    sleep  5s
    
    # Wait Until Page Contains Element  //*[@text=\"Support & Help"\]
    # sleep  2s
    
    # Wait Until Page Contains Element  //*[@class=\"android.view.ViewGroup\"][@index=0]
    # sleep  2s
    
    # Tap  xpath=//*[@class=\"android.view.ViewGroup\"][@index=0]
    # sleep  2s
    
    # Input Text   accessibility_id=reply_ticket_message   ${ReplayMsg}
    # sleep  2s
    
    # Tap  xpath=//*[@text=\"Send"\]
    # sleep  2s
    
    # Tap  xpath=//*[@class=\"android.widget.ImageButton\"][@index=0]
    # sleep  2s
    
    # Wait Until Page Contains Element  //*[@text=\"Support & Help"\]
    # sleep  2s