*** Settings ***
| Suite Setup | Set Library Search Order | AppiumLibrary | Selenium2Library
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Resource     ../../vendor-app-setup.robot
Resource    ../../positive/account/keywords/facepay-vendor-login-positive.robot
Resource    ../../positive/account_security/keywords/choose-security-pin.robot
Resource    keywords/support-help-issue-replay-negative.robot

*** Test Cases ***
Launching FacePay Vendor Application
    Log    Open Application
    Launch FacePay Vendor Application
    
Login Application with Valid Credentials
    Log  Login Application with Valid credentails
    User login with valid credentials
    
Choose Security Pin to Secure the Account
    Log  User need to set the security pin to secure the Account
    Set a Security Pin for First Time

User Will Submit a Response to The Issue in Support & Help 
    Log  User needs to submit the issues in support and help section
    Replay to The Support & Help Issue Without Typing Any Response