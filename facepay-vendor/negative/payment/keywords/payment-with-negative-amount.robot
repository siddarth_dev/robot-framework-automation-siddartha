*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables  ../../../variables/variable.py

*** Keywords ***
User Enter Negative Amount for Payment
    
    Tap  xpath=//*[@text=\"Accept Payment\"]
    sleep  2s
    
    Input text  accessibility_id=accept_payment  ${NegativeAmount}
    sleep  2s
    
    PressKeycode  \13
    Sleep  2s
    
    Tap  xpath=//*[@text=\"Initiate Payment\"]
    sleep  2s
    
    # ${toast}  Get Text  accessibility_id=toast
    # Should Not Be Equal    ${toast}    None    
    # Log    ${toast}  
    
    Page Should Contain Text   Enter a valid amount to continue
    sleep  2s