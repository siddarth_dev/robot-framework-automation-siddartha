*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables  ../../../variables/variable.py

Resource    ../../../positive/security/keywords/enter-security-pin.robot

*** Keywords ***
User Start Paying Less than Limit Amount
    Tap  accessibility_id=enter_pin 
    sleep  2s
    
    Enter Security Pin To Re-enter into App
    sleep  2s
    
    Tap  xpath=//*[@text=\"Accept Payment\"]
    sleep  2s
    
    Input Text  accessibility_id=accept_payment  ${LessAmount}
    sleep  2s
    
    Tap  xpath=//*[@text=\"Initiate Payment\"]
    sleep  2s
    
    # ${toast}  Get Text  accessibility_id=toast 
    
    # Should Not Be Equal    ${toast}    None    
    # Log    ${toast}  
    # sleep  2s 
    
    Page Should Contain Text   Amount should be lesser than 0.5 cents
    
   sleep  2s