*** Settings ***
| Suite Setup | Set Library Search Order | AppiumLibrary | Selenium2Library
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Resource    ../../vendor-app-setup.robot
Resource    ../../positive/account/keywords/sign-out.robot
Resource    ../../positive/account/keywords/facepay-vendor-login-positive.robot
Resource    ../../positive/account_security/keywords/choose-security-pin.robot
Resource    keywords/payment-with-negative-amount.robot
Resource    keywords/payment-with-less-amount.robot


*** Test Cases ***
Launch FacePay Vendor Application
    Log  Launch FacePay Vendor Application
    Launch FacePay Vendor Application
      
User Login with Valid credentials 
    Log  Login with valid Username & Password
    User login with valid credentials
    
Choose Security Pin to Secure the Account
    Log  User need to set the security pin to secure the Account
    Set a Security Pin for First Time
    
Vendor Payment Using Negative Amount
    Log  User try to pay negative amount
    User Enter Negative Amount For Payment
    
Vendor Payment With Less Target Amount
    Log  User will try to pay less amount compare amount limit
    User Start Paying Less than Limit Amount
    
User Sign-out from the Application
    Log  User should sign-out from the application
    User Logout From The Application
    