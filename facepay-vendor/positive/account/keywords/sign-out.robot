*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables  ../../../variables/variable.py



*** Keywords ***
User Logout From The Application
    
    sleep  2s
    
    Tap  accessibility_id=logout_btn    
    sleep  2s
    
    
    Tap  xpath=//*[@text=\"Sign Out\"]
    sleep  2s
    
    Page Should Contain Text  ${LoginWindowTitle}
    sleep  2s
    
    
    
    