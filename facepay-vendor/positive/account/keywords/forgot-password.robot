*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables   ../../../variables/variable.py

Resource    ../../../vendor-app-setup.robot
  
*** Keywords ***
User Login with Forget Password Action And Resend OTP
    
    sleep  5s
    Log  ${LoginWindowTitle}
    Page Should Contain Text  ${LoginWindowTitle}
    
    Click Element  xpath=//*[@text=\"Forgot password\"]
    sleep  4s
    
    Input text  accessibility_id=forgotPassword_email  ${UserMail}
    sleep  2s
    
    Click Element  xpath=//*[@text=\"Reset Password\"]
    sleep  2s
    
    Wait Until Page Contains Element  //*[@text=\"Reset password\"]
    sleep  2s
    
    
    # verify navigated to OTP page
    Page Should Contain Text    Reset password
    Sleep    5s    
    
    # Put FacePay app in background
    Background App    10
    
    # Launch SMS App
    Start Activity  com.google.android.apps.messaging    com.google.android.apps.messaging.ui.ConversationListActivity
    sleep  5s
   
    # Read OTP
    ${latestMessage}     Get Text    id=com.google.android.apps.messaging:id/conversation_snippet
    Log     ${latestMessage}    
    
    # Verify OTP came from FacePay messaging service 
    Should Contain    ${latestMessage}    is your verification code for FacePay 
    
    # Extract only otp from message
    @{otp}     Split String   ${latestMessage}     
    Log    @{otp}[0] 
    
    
    # Switch back to FacePay app
    Press Keycode    187
    Click Element    accessibility_id=FacePay Vendor   
     
    Sleep    2s
    
  
    # Verify switched to Facepay app
    Page Should Contain Text    Reset password
    
    
    Input Text  accessibility_id=otp_code  @{otp}[0]
    sleep  2s
    
    Input Text  accessibility_id=new_password  ${NewForgotPass}
    sleep  2s
    
    Input Text  accessibility_id=confirm_password  ${NewForgotPass}
    sleep  2s
    
    
    Tap  xpath=//*[@text=\"Save password\"]
    sleep  2s
    
    