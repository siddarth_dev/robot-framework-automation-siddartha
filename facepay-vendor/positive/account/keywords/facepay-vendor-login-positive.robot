*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables   ../../../variables/variable.py


*** Keywords ***

User login with valid credentials 
    
    sleep  5s
    Log  ${LoginWindowTitle}
    Page Should Contain Text  ${LoginWindowTitle}
    
    Input text  accessibility_id=login_email         ${ValidEmail}
    Input text  accessibility_id=login_password      ${ValidPassword}
    sleep  2s
    
    Click Element  xpath=//*[@text=\"Login\"]
    sleep  8s
    
    Wait Until Page Contains Element  //*[@text=\"Choose Security pin\"]
    sleep  2s
    