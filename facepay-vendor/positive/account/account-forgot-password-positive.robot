*** Settings ***
| Suite Setup | Set Library Search Order | AppiumLibrary | Selenium2Library
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Resource    ../../vendor-app-setup.robot
Resource    keywords/forgot-password.robot

*** Test Cases ***
Launch FacePay Vendor Application
    Log  Launch FacePay Vendor Application
    Launch FacePay Vendor Application
      
User Can Reset Password Using Forgot Password Option
    Log  User needs to set new password
    User Login with Forget Password Action And Resend OTP