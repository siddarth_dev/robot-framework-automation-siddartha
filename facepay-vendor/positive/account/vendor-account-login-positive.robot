*** Settings ***
| Suite Setup | Set Library Search Order | AppiumLibrary | Selenium2Library
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Resource    keywords/facepay-vendor-login-positive.robot
Resource    ../../vendor-app-setup.robot
Resource    ../account_security/keywords/choose-security-pin.robot
Resource    keywords/sign-out.robot


*** Test Cases ***
Launch FacePay Vendor Application
    Log  Launch FacePay Vendor Application
    Launch FacePay Vendor Application
    
    
User Login with Valid credentials 
    Log  Login with valid Username & Password
    User login with valid credentials
    
Choose Security Pin to Secure the Account
    Log  User need to set the security pin to secure the Account
    Set a Security Pin for First Time
    
User Sign-out from the Application
    Log  User should sign-out from the application
    User Logout From The Application
    