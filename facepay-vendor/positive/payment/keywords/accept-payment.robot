*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables   ../../../variables/variable.py

Resource    ../../account_security/keywords/enter-security-pin.robot

*** Keywords ***
User can pay amount to vendor
    
    sleep  2s
    
    Tap  xpath=//*[@text=\"Accept Payment\"]
    sleep  2s
    
    Input Text  accessibility_id=accept_payment  ${Amount}
    sleep  2s
    
    Tap   xpath=//*[@text=\"Initiate Payment\"]
    sleep  2s
    
    Tap  xpath=//*[@text=\" Cancel \"]
    sleep  2s
    
    Tap  accessibility_id=go_back
    sleep  2s
    
    Tap  accessibility_id=enter_pin
    sleep  4s
    
    Wait Until Page Contains Element  //*[@text=\"Enter Security pin\"]
    sleep  3s
    
    Enter Security Pin To Re-enter into App
    sleep  2s
    
    
    Wait Until Page Contains Element  //*[@text=\"Recent Transactions\"]
    sleep  2s