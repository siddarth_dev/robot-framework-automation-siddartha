*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables   ../../../variables/variable.py


*** Keywords ***
Change a Security Pin
    
    sleep  2s
    
    Click element  accessibility_id=logout_btn
    sleep  2s
    
    Click element  xpath=//*[@text=\"Change Pin"\]
    sleep  2s
    
    Input text  accessibility_id=old_pin   ${OldPin}
    sleep  2s
    
    Input text  accessibility_id=new_pin   ${NewPin}
    sleep  2s
    
    Click element  xpath=//*[@text=\"Change\"]
    sleep  5s
    
    Wait Until Page Contains Element  //*[@text=\"Recent Transactions\"]
    sleep  2s
    
    
    # Should Not Equal To   ${toast}  None
    # sleep  2s
    
    