*** Settings ***
Library     AppiumLibrary    10
Library     String    
Library     OperatingSystem 
Variables   ../../../variables/variable.py

Resource    ../../account/keywords/facepay-vendor-login-positive.robot

*** Keywords ***
Set a Security Pin for First Time
    
    sleep  2s
    
    Input text  accessibility_id=pin   ${SecurityCode}
    sleep  2s
    
    Input text  accessibility_id=confirm_pin   ${SecurityCode}
    sleep  2s
    
    Click element  xpath=//*[@text=\"Set"\]
    sleep  5s
    
    # ${toast}   Get Text  accessibility_id=toast
    # sleep  5s
    
    Wait Until Page Contains Element  //*[@text=\"Recent Transactions\"]
    sleep  2s