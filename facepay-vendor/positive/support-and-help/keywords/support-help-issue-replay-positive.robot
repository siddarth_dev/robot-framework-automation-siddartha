*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables   ../../../variables/variable.py


*** Keywords ***
Replay a Message to The Support & Help Issue
    
    Click element  accessibility_id=logout_btn
    sleep  2s
    
    Click element  xpath=//*[@text=\"Support & Help"\]
    sleep  4s
    
    Wait Until Page Contains Element  //*[@class=\"android.view.ViewGroup\"][@index=0]
    sleep  2s
    
    Tap  xpath=//*[@class=\"android.view.ViewGroup\"][@index=0]
    sleep  2s
    
    Input Text   accessibility_id=reply_ticket_message   ${ReplayMsg}
    sleep  2s
    
    Tap  xpath=//*[@text=\"Send"\]
    sleep  2s
    
    Tap  xpath=//*[@class=\"android.widget.ImageButton\"][@index=0]
    sleep  2s
    
    Tap  xpath=//*[@class=\"android.widget.ImageButton\"][@index=0]
    sleep  2s
    
    Wait Until Page Contains   Recent Transactions
    sleep  2s