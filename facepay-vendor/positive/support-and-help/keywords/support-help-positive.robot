*** Settings ***
Library    AppiumLibrary    10
Library    String    
Library    OperatingSystem 
Variables   ../../../variables/variable.py


*** Keywords ***
Report a Issue in Support & Help
    
    Click element  accessibility_id=logout_btn
    sleep  2s
    
    Click element  xpath=//*[@text=\"Support & Help"\]
    sleep  4s
    
    Wait Until Page Contains Element  //*[@text=\"Report new Issue"\]
    sleep  2s
    
    Tap  xpath=//*[@text=\"Report new Issue"\]
    sleep  2s
    
    Click element  xpath=//*[@text=\"Can't make any transactions"\]
    sleep  1s
    
    Click element  xpath=//*[@text=\"Face is not recognizing properly"\]
    sleep  1s
    
    Input text  accessibility_id=create_ticket_summary   ${SupportMsg}
    
    Click element  xpath=//*[@text=\"Report"\]
    sleep  2s
    
    Wait Until Page Contains Element  //*[@text=\"Support & Help"\]
    sleep  2s
    
    Tap  xpath=//*[@class=\"android.widget.ImageButton\"][@index=0]
    sleep  2s
    
    Wait Until Page Contains   Recent Transactions
    sleep  2s
    
    
