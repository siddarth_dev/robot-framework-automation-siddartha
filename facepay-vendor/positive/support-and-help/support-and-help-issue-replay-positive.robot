*** Settings ***
| Suite Setup | Set Library Search Order | AppiumLibrary | Selenium2Library
Library  AppiumLibrary  10
Library    String    
Library    OperatingSystem 

Resource    ../../vendor-app-setup.robot
Resource    ../account/keywords/sign-out.robot
Resource    ../account/keywords/facepay-vendor-login-positive.robot
Resource    ../account_security/keywords/choose-security-pin.robot
Resource    keywords/support-help-issue-replay-positive.robot

*** Test Cases ***
Launching FacePay Vendor Application
    Log    Open Application
    Launch FacePay Vendor Application
    
Login Application with Valid Credentials
    Log  Login Application with Valid credentails
    User login with valid credentials
    
Choose Security Pin to Secure the Account
    Log  User need to set the security pin to secure the Account
    Set a Security Pin for First Time

User Will Submit a Response to The Issue in Support & Help 
    Log  User needs to submit the issues in support and help section
    Replay a Message to The Support & Help Issue
    
User Sign-out from the Application
    Log  User should sign-out from the application
    User Logout From The Application