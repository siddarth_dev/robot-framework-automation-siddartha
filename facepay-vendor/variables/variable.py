def get_variables(arg=None):
    variables = {
    'var' : 'value',
    'LoginWindowTitle':        u'Welcome to FacePay Vendor!',
    'AppName':                 u'FacePay Vendor!',
    'ValidEmail':              u'harshamay0.6@gmail.com',
    'ValidPassword':           u'654321',
    'PhoneNumber':             u'8861835846',
    'NoEmail':                 u'',
    'NoPassword':              u'',
    'UserMail':                u'asiddarth86@gmail.com',
    'NewPhoneNumber':          u'7019847480',  
    'InvalidEmail':            u'a$i&&@rth86@gmail.com',
    'InvalidPassword':         u'123456',
    'Clickable':               u'false',
    'SupportMsg':              u'Testing: Face looks awkward',
    'SecurityCode':            u'1234',
    'OldPin':                  u'1234',
    'NewPin':                  u'4321',
    'Amount':                  u'1',
    'ReplayMsg':               u'We will get back to you',
    'NegativeAmount':          u'-5',
    'LessAmount':              u'0.2',
    'WrongSecurityCode':       u'2341',
    'NewForgotPass':           u'432175',
    'NoIssueMsg':              u'',
    'ReplayEmtMsg':            u''
    }
    return variables
